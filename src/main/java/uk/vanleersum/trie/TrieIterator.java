/*******************************************************************************
 * Copyright 2018 Dr Phill van Leersum
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package uk.vanleersum.trie;

import java.util.Iterator;
import java.util.NoSuchElementException;

/*package*/ final class TrieIterator<T> implements Iterator<IndexTrie<T>> {
	private IndexTrie<T> current;

	/* package */ TrieIterator(final IndexTrie<T> start) {
		this.current = start;
	}

	@Override
	public final boolean hasNext() {
		return null != this.current;
	}

	@Override
	public final IndexTrie<T> next() {
		if (null == this.current) {
			throw new NoSuchElementException();
		}
		final IndexTrie<T> next = this.current;
		this.current = this.current.nextTrie();
		return next;
	}

}
