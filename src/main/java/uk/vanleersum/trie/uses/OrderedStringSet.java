/*******************************************************************************
 * Copyright 2018 Dr Phill van Leersum
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package uk.vanleersum.trie.uses;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import uk.vanleersum.trie.IndexTrie;

public class OrderedStringSet implements Set<String> {
	private IndexTrie<String> trie = new IndexTrie<>();
	private final int[] map;

	public OrderedStringSet() {
		this(null);
	}

	public OrderedStringSet(final String content) {
		if (null == content) {
			this.map = null;
		} else {
			int max = 0;
			for (int i = 0; i < content.length(); i++) {
				final char charAt = content.charAt(i);
				if (charAt > max) {
					max = charAt;
				}
			}
			this.map = new int[max + 1];
			for (int i = 0; i < content.length(); i++) {
				final char charAt = content.charAt(i);
				this.map[charAt] = i;
			}
		}
	}

	@Override
	public final boolean add(final String string) {
		this.trie.putTrie(new StringIndexer(string, this.map)).value = string;
		return true;
	}

	@Override
	public boolean addAll(final Collection<? extends String> c) {
		for (final String string : c) {
			this.add(string);
		}
		return true;
	}

	@Override
	public final void clear() {
		this.trie.clear();
	}

	@Override
	public final boolean contains(final Object o) {
		if (o instanceof String) {
			final String string = (String) o;
			return this.contains(string);
		}
		return false;
	}

	public final boolean contains(final String string) {
		final IndexTrie<String> t = this.trie.getTrie(new StringIndexer(string, this.map));
		if (null != t) {
			return true;
		}
		return false;
	}

	@Override
	public boolean containsAll(final Collection<?> c) {
		for (final Object o : c) {
			if (!this.contains(o)) {
				return false;
			}
		}
		return true;
	}

	public String debugString() {
		return this.trie.debugString();
	}

	@Override
	public boolean isEmpty() {
		return 0 == this.size();
	}

	@Override
	public final Iterator<String> iterator() {
		return this.trie.iterator();
	}

	@Override
	public final boolean remove(final Object o) {
		if (o instanceof String) {
			this.remove((String) o);
			return true;
		}
		return false;
	}

	public final void remove(final String string) {
		final IndexTrie<String> t = this.trie.getTrie(new StringIndexer(string, this.map));
		if (null != t) {
			t.remove();
		}
	}

	@Override
	public boolean removeAll(final Collection<?> c) {
		for (final Object o : c) {
			this.remove(o);
		}
		return true;
	}

	@Override
	public boolean retainAll(final Collection<?> c) {
		for (final String string : this.trie.list()) {
			if (!c.contains(string)) {
				this.remove(string);
			}
		}
		return false;
	}

	@Override
	public final int size() {
		return this.trie.size();
	}

	/**
	 * @param prefix
	 * @return an ordered list of the strings that begin with the supplied prefix.
	 */
	public final List<String> stringsWithPrefix(final String prefix) {
		final IndexTrie<String> t = this.trie.getTrie(new StringIndexer(prefix));
		if (null == t) {
			return new ArrayList<>();
		}
		return t.list();
	}

	@Override
	public final Object[] toArray() {
		return this.trie.list().toArray();
	}

	@Override
	public final <T> T[] toArray(final T[] a) {
		return this.trie.list().toArray(a);
	}
}
