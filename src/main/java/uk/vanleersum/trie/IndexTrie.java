/*******************************************************************************
 * Copyright 2018 Dr Phill van Leersum
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package uk.vanleersum.trie;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * An implementation of a trie where the positions of the values are defined by
 * a series of integers (An index path). These series of integers are
 * represented by instances of {@link Iterator}<{@link Integer}>. <br>
 * Using these indexers values of type T can be inserted, removed, or retrieved.
 * <br>
 * A trie can be considered strictly ordered by a depth-first tree traversal -
 * the order being entirely defined by the values in the index path.
 *
 * @author Dr Phill van Leersum
 *
 * @param <T>
 *
 */
public class IndexTrie<T> implements Iterable<T> {
	public T value = null;

	private final IndexTrie<T> parent;
	private final int indexValue;
	private IndexTrie<T> firstChild = null;
	private IndexTrie<T> nextSibling = null;
	private List<Integer> compactList = null;

	/**
	 * Create a new instance of IndexTrie containing null at root.
	 * <p>
	 *
	 * @param useCompaction
	 */
	public IndexTrie() {
		this(null, 0, null, null);
	}

	/**
	 * Create a new instance of IndexTrie containing the given value at root.
	 * <p>
	 * This is equivalent to calling the three argument constructor with (null,
	 * value, true)
	 *
	 * @param value
	 */
	public IndexTrie(final T value) {
		this(null, 0, null, value);
	}

	private IndexTrie(final IndexTrie<T> parent, final int indexValue, final Iterator<Integer> indexer, final T value) {
		this.parent = parent;
		this.indexValue = indexValue;
		if ((null != indexer) && indexer.hasNext()) {
			this.compactList = new ArrayList<>();
			while (indexer.hasNext()) {
				this.compactList.add(indexer.next());
			}
			this.value = value;
		} else {
			// we ARE the target
			this.value = value;
		}
	}

	/**
	 * @return the number of direct children of the receiver
	 */
	public final int childCount() {
		int count = 0;
		IndexTrie<T> child = this.firstChild;
		while (null != child) {
			count++;
			child = child.nextSibling;
		}
		return count;
	}

	/**
	 * Remove the all contents of the receiver. After this call the receiver will
	 * have no value, no children.
	 */
	public final void clear() {
		this.value = null;
		this.firstChild = null;
		this.compactList = null;
	}

	/**
	 * @return a string representing the state of the receiver. This will be
	 *         arranged as an indented tree, and mat be quit large.
	 */
	public final String debugString() {
		final StringBuilder buf = new StringBuilder();
		this.writeOn(buf, "");
		return buf.toString();
	}

	/**
	 * Return the trie at the specified index path or null if none exists
	 *
	 * @param indexer
	 * @return the trie found or null if none exists
	 */
	public IndexTrie<T> getTrie(final Iterator<Integer> indexer) {
		if (null != this.compactList) {
			final Iterator<Integer> compactIterator = this.compactList.iterator();
			Integer index = -1;
			while (compactIterator.hasNext()) {
				if (!indexer.hasNext()) {
					return null; // does not exist
				}
				final Integer compact = compactIterator.next();
				index = indexer.next();
				if (!compact.equals(index)) {
					return null; // does not exist
				}
			}
		}
		if (!indexer.hasNext()) { // we ARE the target
			return this;
		}

		final int index = indexer.next(); // which of our children

		if (null == this.firstChild) {
			return null; // no children - do not have have the target
		}

		final IndexTrie<T> child = this.childWithIndexValue(index);
		if (null == child) { // we do not have the target
			return null;
		}
		return child.getTrie(indexer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public final Iterator<T> iterator() {
		return new ValueIterator<>(this);
	}

	/**
	 * @return a {@link List}<T> of the contents of the receiver in logical order
	 *         (depth first traversal). This list is a snapshot and NOT backed by
	 *         the Trie. Changes to the Trie will not be reflected in the list
	 *         returned.
	 */
	public final List<T> list() {
		return this.list(new ArrayList<>());
	}

	/**
	 * @param list
	 * @return the supplied list with the contents of this tree added in logical
	 *         order (depth first traversal).This list is a snapshot and NOT backed
	 *         by the Trie. Changes to the Trie will not be reflected in the list
	 *         returned.
	 */
	public final List<T> list(final List<T> list) {
		if (null != this.value) {
			list.add(this.value);
		}
		IndexTrie<T> child = this.firstChild;
		while (null != child) {
			child.list(list);
			child = child.nextSibling;
		}
		return list;
	}

	/**
	 * @return the next trie from the receiver in depth-first recursive mode, or
	 *         null if there is none.
	 *         <p>
	 *         Continuously calling nextTrie on the result will traverse the entire
	 *         trie.
	 */
	public final IndexTrie<T> nextTrie() {
		if (null != this.firstChild) {
			return this.firstChild;
		}
		return this.nextTrieUp();
	}

	/**
	 * Return the Trie at the specified index path. If no Trie exists at the path
	 * then create one and return it;
	 *
	 * @param indexer
	 * @return the Trie found or created
	 */
	public final IndexTrie<T> putTrie(final Iterator<Integer> indexer) {
		if (null != this.compactList) {
			// bleed the values from it and check that they match the incoming enumerator
			// if we run out of our values first then create a child with the remainder of
			// the indexer
			// if we run out of indexer values first then create two children
			// - one for the remainder of our values (shorten compactList)
			// - one for remainder of indexer (will happen automatically in rest of method)
			// if indexer matches compactList then replace our value
			final Iterator<Integer> compactIterator = this.compactList.iterator();
			int count = 0;
			while (compactIterator.hasNext()) {
				final Integer compact = compactIterator.next();
				if (!indexer.hasNext()) {
					// indexer has run out of values so we are the target. But as we have
					// a compact list that continues, we need to create a child for the residue
					this.breakCompactList(compact, compactIterator, count, compact);
					return this; // since we were the target
				}

				final Integer index = indexer.next();

				if (!compact.equals(index)) {
					this.breakCompactList(compact, compactIterator, count, compact);

					// create a new child to represent the new value
					final IndexTrie<T> newChild = new IndexTrie<>(this, index, indexer, this.value);
					this.insertChild(newChild);
					return newChild;
				}
				count++;
			}
		}

		if (!indexer.hasNext()) { // we ARE the target
			return this;
		}

		final int index = indexer.next();

		final IndexTrie<T> existingChild = this.childWithIndexValue(index);
		if (null == existingChild) {
			// add new child and return it.
			final IndexTrie<T> newChild = new IndexTrie<>(this, index, indexer, this.value);
			this.insertChild(newChild);
			return newChild;
		} else {
			// return the identity of a found/created trie
			return existingChild.putTrie(indexer);
		}
	}

	public final void remove() {
		if (null == this.parent) {
			return;
		}
		if (this == this.parent.firstChild) {
			this.parent.firstChild = this.nextSibling;
			return;
		}

		IndexTrie<T> child = this.parent.firstChild;
		while (null != child) {
			if (this == child.nextSibling) {
				child.nextSibling = this.nextSibling;
				return;
			}
			child = child.nextSibling;
		}
	}

	/**
	 * @return the number of T stored in the receiver
	 */
	public final int size() {
		int size = (null == this.value) ? 0 : 1;
		IndexTrie<T> child = this.firstChild;
		while (null != child) {
			size += child.size();
			child = child.nextSibling;
		}
		return size;
	}

	public final Stream<T> stream() {
		return StreamSupport.stream(
				Spliterators.spliterator(this.iterator(), 0L, Spliterator.NONNULL | Spliterator.ORDERED),
				/* not parallel */ false);
	}

	/**
	 * @return an iterator on the Trie<T> represented by this node. Tries are
	 *         iterated in depth-first recursive order, ensuring that they are
	 *         ordered by the semantics implied by the indexer.
	 */
	public final Iterator<IndexTrie<T>> tries() {
		return new TrieIterator<>(this);
	}

	/* package */ final Depth depth(final Iterator<Integer> indexer) {
		return this.depth(indexer, new Depth());
	}

	/* package */ void writeOn(final StringBuilder buf, final String indent) {
		buf.append(indent);
		buf.append("[");
		buf.append(this.indexValue);
		buf.append("]");
		if (null == this.compactList) {
			// buf.append("[] ");
		} else {
			buf.append(this.compactList.toString());
		}
		buf.append(" ");
		if (null == this.value) {
			buf.append("<null>");
		} else {
			buf.append(this.value.toString());
		}
		buf.append("  {");
		buf.append(this.hashCode());
		buf.append('}');
		IndexTrie<T> child = this.firstChild;
		while (null != child) {
			buf.append("\n");
			child.writeOn(buf, indent + "....");
			child = child.nextSibling;
		}
	}

	/*
	 * Break the compact list at the given count and create a new child containing
	 * our original children and value. Remove our children and add the new child
	 * representing the remainder of the compact list
	 *
	 * @param compactIterator
	 *
	 * @param count
	 *
	 * @param compact
	 */
	// TODO: inline this?
	private final void breakCompactList(final int indexValue, final Iterator<Integer> compactIterator, final int count,
			final Integer compact) {
		final IndexTrie<T> childForRemainderOfCompactList = new IndexTrie<>(this, indexValue, compactIterator,
				this.value);
		childForRemainderOfCompactList.firstChild = this.firstChild;
		this.firstChild = childForRemainderOfCompactList;
		this.value = null;
		if (0 == count) {
			// we are at the first position in our original compact list so our compact list
			// can be discarded
			this.compactList = null;
		} else {
			// We need to shorten the new compact list by the amount that we used for the
			// new child
			this.compactList = this.compactList.subList(0, count);
		}
	}

	private final IndexTrie<T> childWithIndexValue(final int indexValue) {
		IndexTrie<T> child = this.firstChild;
		while ((null != child) && (child.indexValue <= indexValue)) {
			if (child.indexValue == indexValue) {
				return child;
			}
			child = child.nextSibling;
		}
		return null;
	}

	private final Depth depth(final Iterator<Integer> indexer, final Depth depth) {
		if (null != this.compactList) {
			final Iterator<Integer> compactIterator = this.compactList.iterator();
			Integer index = -1;
			while (compactIterator.hasNext()) {
				if (!indexer.hasNext()) {
					return depth; // does not exist
				}
				final Integer compact = compactIterator.next();
				index = indexer.next();

				if (!compact.equals(index)) {
					return depth; // does not exist
				}
				depth.logical += 1;
			}
		}
		if (!indexer.hasNext()) { // we ARE the target
			depth.found = true;
			return depth;
		}

		final int index = indexer.next(); // which of our children

		final IndexTrie<T> child = this.childWithIndexValue(index);
		if (null == child) { // we do not have the target
			return depth;
		}
		depth.logical++;
		depth.physical++;
		return child.depth(indexer, depth);
	}

	private final void insertChild(final IndexTrie<T> insertChild) {
		if (null == this.firstChild) {
			this.firstChild = insertChild;
			return;
		}
		if (this.firstChild.indexValue >= insertChild.indexValue) {
			insertChild.nextSibling = this.firstChild;
			this.firstChild = insertChild;
			return;
		}

		IndexTrie<T> child = this.firstChild;
		while (null != child) {
			if (null == child.nextSibling) {
				child.nextSibling = insertChild;
				return;
			}
			if (child.nextSibling.indexValue > insertChild.indexValue) {
				insertChild.nextSibling = child.nextSibling;
				child.nextSibling = insertChild;
				return;
			}
			child = child.nextSibling;
		}
		System.out.println("IndexTrie.insertChild: Should not get here"); // DEBUG
	}

	private final IndexTrie<T> nextTrieUp() {
		// return logically next trie without going downwards
		if (null != this.nextSibling) {
			return this.nextSibling;
		}
		if (null != this.parent) {
			return this.parent.nextTrieUp();
		}
		return null;
	}
}
