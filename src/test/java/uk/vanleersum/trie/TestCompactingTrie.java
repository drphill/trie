/*******************************************************************************
 * Copyright 2018 Dr Phill van Leersum
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package uk.vanleersum.trie;

import org.junit.Assert;
import org.junit.Test;

import uk.vanleersum.trie.uses.StringIndexer;

public class TestCompactingTrie {

	@Test
	public void test1() {
		// test that adding a substring of a compactList correctly splits the
		// compactList
		final IndexTrie<String> trie = new IndexTrie<>();
		
		
		trie.putTrie(new StringIndexer("123")).value = "123";
		IndexTrie<String> got = trie.getTrie(new StringIndexer("123")) ;
		System.out.println("TestCompactingTrie.test1: "+ trie.debugString()); //DEBUG
		Assert.assertNotNull(got);
		Assert.assertEquals("123", got.value);
		
		trie.putTrie(new StringIndexer("124")).value = "124";
		got = trie.getTrie(new StringIndexer("124")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("124", got.value);
		
		trie.putTrie(new StringIndexer("0124")).value = "0124";
		got = trie.getTrie(new StringIndexer("0124")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("0124", got.value);
		
		
		trie.putTrie(new StringIndexer("012")).value = "012";
		got = trie.getTrie(new StringIndexer("012")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("012", got.value);

	}


	@Test
	public void test3() {
		// test that adding a substring of a compactList correctly splits the
		// compactList
		final IndexTrie<String> trie = new IndexTrie<>();

		trie.putTrie(new StringIndexer("0123456789")).value = "0123456789";
		Depth depth = trie.depth(new StringIndexer("0123456789"));
		Assert.assertTrue(depth.found);
		Assert.assertTrue(10 == depth.logical);
		Assert.assertTrue(1 == depth.physical);
		
		IndexTrie<String> got = trie.getTrie(new StringIndexer("0123456789")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("0123456789", got.value);

		trie.putTrie(new StringIndexer("0123356789")).value = "0123356789";

		depth = trie.depth(new StringIndexer("0123456789"));
		Assert.assertTrue(depth.found);
		Assert.assertTrue(10 == depth.logical);
		Assert.assertTrue(2 == depth.physical);
		
		got = trie.getTrie(new StringIndexer("0123456789")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("0123456789", got.value);

		depth = trie.depth(new StringIndexer("0123356789"));
		Assert.assertTrue(depth.found);
		Assert.assertTrue(10 == depth.logical);
		Assert.assertTrue(2 == depth.physical);
		
		got = trie.getTrie(new StringIndexer("0123356789")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("0123356789", got.value);

		// System.out.println(trie.debugString());

		// Split the already split string
		trie.putTrie(new StringIndexer("0123455789")).value = "0123455789";
		// System.out.println(trie.debugString());

		depth = trie.depth(new StringIndexer("0123456789"));
		Assert.assertTrue(depth.found);
		Assert.assertTrue(10 == depth.logical);
		Assert.assertTrue(3 == depth.physical);
		
		got = trie.getTrie(new StringIndexer("0123456789")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("0123456789", got.value);

		depth = trie.depth(new StringIndexer("0123356789"));
		Assert.assertTrue(depth.found);
		Assert.assertTrue(10 == depth.logical);
		Assert.assertTrue(2 == depth.physical);
		
		got = trie.getTrie(new StringIndexer("0123356789")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("0123356789", got.value);

		depth = trie.depth(new StringIndexer("0123455789"));
		Assert.assertTrue(depth.found);
		Assert.assertTrue(10 == depth.logical);
		Assert.assertTrue(3 == depth.physical);
		
		got = trie.getTrie(new StringIndexer("0123455789")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("0123455789", got.value);
	}

	@Test
	public void test4() {
		// test that adding a substring of a compactList correctly splits the
		// compactList
		final IndexTrie<String> trie = new IndexTrie<>();

		trie.putTrie(new StringIndexer("0123456789")).value = "0123456789";
		Depth depth = trie.depth(new StringIndexer("0123456789"));
		Assert.assertTrue(depth.found);
		Assert.assertTrue(10 == depth.logical);
		Assert.assertTrue(1 == depth.physical);
		
		IndexTrie<String> got = trie.getTrie(new StringIndexer("0123456789")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("0123456789", got.value);

		trie.putTrie(new StringIndexer("1123356789")).value = "1123356789";

		depth = trie.depth(new StringIndexer("0123456789"));
		Assert.assertTrue(depth.found);
		Assert.assertTrue(10 == depth.logical);
		Assert.assertTrue(1 == depth.physical);
		
		got = trie.getTrie(new StringIndexer("0123456789")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("0123456789", got.value);

		depth = trie.depth(new StringIndexer("1123356789"));
		Assert.assertTrue(depth.found);
		Assert.assertTrue(10 == depth.logical);
		Assert.assertTrue(1 == depth.physical);
		
		got = trie.getTrie(new StringIndexer("1123356789")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("1123356789", got.value);
	}

	@Test
	public void test5() {
		// test that adding a supersstring of a compactList correctly splits the
		// compactList
		final IndexTrie<String> trie = new IndexTrie<>();

		trie.putTrie(new StringIndexer("0123")).value = "0123";
		Depth depth = trie.depth(new StringIndexer("0123"));
		Assert.assertTrue(depth.found);
		Assert.assertTrue(4 == depth.logical);
		Assert.assertTrue(1 == depth.physical);
		IndexTrie<String> got = trie.getTrie(new StringIndexer("0123")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("0123", got.value);

		trie.putTrie(new StringIndexer("0123356789")).value = "0123356789";

		depth = trie.depth(new StringIndexer("0123"));
		Assert.assertTrue(depth.found);
		Assert.assertTrue(4 == depth.logical);
		Assert.assertTrue(1 == depth.physical);
		
		got = trie.getTrie(new StringIndexer("0123")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("0123", got.value);

		depth = trie.depth(new StringIndexer("0123356789"));
		Assert.assertTrue(depth.found);
		Assert.assertTrue(10 == depth.logical);
		Assert.assertTrue(2 == depth.physical);
		got = trie.getTrie(new StringIndexer("0123356789")) ;
		Assert.assertNotNull(got);
		Assert.assertEquals("0123356789", got.value);
	}

}
