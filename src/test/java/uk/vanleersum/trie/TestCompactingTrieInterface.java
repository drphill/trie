/*******************************************************************************
 * Copyright 2018 Dr Phill van Leersum
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package uk.vanleersum.trie;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;

import uk.vanleersum.trie.uses.OrderedStringSet;

public class TestCompactingTrieInterface {

	@Test
	public void testAddAll() {
		final Collection<String> collection = new ArrayList<>();
		collection.add("123foo");
		collection.add("12bar");
		collection.add("poot");
		collection.add("poo");
		collection.add("123foo");
		collection.add("123foo");
		collection.add("12bar");

		final OrderedStringSet trie = new OrderedStringSet();
		Assert.assertTrue(trie.size() == 0);
		trie.addAll(collection);

		Assert.assertTrue(trie.size() == 4);
		Assert.assertTrue(trie.contains("123foo"));
	}

	@Test
	public void testAddAndRemove() {
		final OrderedStringSet trie = new OrderedStringSet();
		Assert.assertTrue(trie.size() == 0);
		trie.add("123foo");
		trie.add("12bar");
		trie.add("poot");
		trie.add("poo");
		trie.add("123foo");
		trie.add("123foo");
		trie.add("12bar");

		Assert.assertTrue(trie.size() == 4);
		Assert.assertTrue(trie.contains("123foo"));

		trie.remove("123foo");
		Assert.assertTrue(trie.size() == 3);
		Assert.assertFalse(trie.contains("123foo"));
		trie.remove("123foo");
		Assert.assertTrue(trie.size() == 3);
		Assert.assertFalse(trie.contains("123foo"));

		trie.remove("123foo");
		Assert.assertTrue(trie.size() == 3);
		Assert.assertFalse(trie.contains("123foo"));
	}

	@Test
	public void testClear() {

		final OrderedStringSet trie = new OrderedStringSet();
		Assert.assertTrue(trie.size() == 0);
		trie.add("123foo");
		trie.add("12bar");
		trie.add("poot");
		trie.add("poo");
		trie.add("123foo");
		trie.add("123foo");
		trie.add("12bar");
		Assert.assertTrue(trie.size() == 4);
		Assert.assertTrue(trie.contains("123foo"));

		trie.clear();
		Assert.assertTrue(trie.size() == 0);
		Assert.assertFalse(trie.contains("123foo"));
	}

	@Test
	public void testContainsAll() {
		final Collection<String> collection = new ArrayList<>();
		collection.add("123foo");
		collection.add("12bar");
		collection.add("poot");
		collection.add("poo");
		collection.add("123foo");
		collection.add("123foo");
		collection.add("12bar");

		final OrderedStringSet trie = new OrderedStringSet();
		Assert.assertTrue(trie.size() == 0);
		trie.addAll(collection);

		Assert.assertTrue(trie.containsAll(collection));

		collection.add("unseen");
		Assert.assertFalse(trie.containsAll(collection));
	}

	@Test
	public void testIterator() {
		final Collection<String> collection = new ArrayList<>();
		collection.add("123foo");
		collection.add("12bar");
		collection.add("poot");
		collection.add("poo");
		collection.add("123foo");
		collection.add("123foo");
		collection.add("12bar");
		collection.add("barz");
		collection.add("baz");
		collection.add("fgjkasd");
		collection.add("fgjzzzz");

		final OrderedStringSet trie = new OrderedStringSet();
		Assert.assertTrue(trie.size() == 0);
		trie.addAll(collection);
		Assert.assertTrue(trie.size() == 8);

		final Collection<String> right = new ArrayList<>();
		right.add("123foo");
		right.add("12bar");
		right.add("barz");
		right.add("baz");
		right.add("fgjkasd");
		right.add("fgjzzzz");
		right.add("poo");
		right.add("poot");

		final Iterator<String> rightIterator = right.iterator();
		final Iterator<String> testIterator = trie.iterator();

		while (testIterator.hasNext()) {
			Assert.assertTrue(rightIterator.hasNext());
			final String testNext = testIterator.next();
			final String rightNext = rightIterator.next();
			Assert.assertEquals(testNext, rightNext);
		}
		Assert.assertEquals(testIterator.hasNext(), rightIterator.hasNext());

	}

	@Test
	public void testRealData() {
		final String combined = "UK0123456 00020,UK0123456 00021,UK0123456 00022,UK0123456 00023,"
				+ "UK0123456 00024,UK0123456 00025,UK0123456 00026,UK0123456 00027,"
				+ "UK0123456 00028,UK0123456 00029,UK0123456 00030,UK0123456 00031,"
				+ "UK0123456 00032,UK0123456 00033,UK0123456 00034,UK0123456 00035,"
				+ "UK0123456 00036,UK0123456 00037,UK0123456 00038,UK0123456 00039,"
				+ "UK0123456 00040,UK0123456 00041,UK0123456 00042,UK0123456 00043,"
				+ "UK0123456 00044,UK0123456 00045,UK0123456 00046,UK0123456 00047,"
				+ "UK0123456 00048,UK0123456 00049,UK0123456 00050,UK0123456 00051,"
				+ "UK0123456 00052,UK0123456 00053,UK0123456 00054,UK0123456 00055,"
				+ "UK0123456 00056,UK0123456 00057,UK0123456 00058,UK0123456 00059,"
				+ "UK0123456 00060,UK0123456 00061,UK0123456 00062,UK0123456 00063,"
				+ "UK0123456 00064,UK0123456 00065,UK0123456 00066,UK0123456 00067,"
				+ "UK0123456 00068,UK0123456 00069,UK0123456 00071,UK0123456 00072,"
				+ "UK0123456 00073,UK0123456 00074,UK0123456 00075,UK0123456 00076,"
				+ "UK0123456 00077,UK0123456 00078,UK0123456 00079,UK0123456 00080,"
				+ "UK0123456 00081,UK0123456 00082,UK0123456 00083,UK0123456 00084,"
				+ "UK0123456 00085,UK0123456 00086,UK0123456 00087,UK0123456 00088,"
				+ "UK0123456 00089,UK0123456 00090,UK0123456 00091,UK0123456 00093,"
				+ "UK0123456 00094,UK0123456 00095,UK0123456 00096,UK0123456 00097,"
				+ "UK0123456 00098,UK0123456 00099,UK0123456 00100,UK0123456 00070," + "UK0123456 00092";

		final OrderedStringSet trie = new OrderedStringSet();
		final String[] broke = combined.split(",");
		for (final String str : broke) {
			trie.add(str);
		}
		System.out.println("TestCompactingTrieInterface.testAddAll: " + trie.size()); // DEBUG

		final boolean b = trie.contains("UK0123456 00070");
		System.out.println("TestCompactingTrieInterface.testAddAll: " + b); // DEBUG
	}

	@Test
	public void testRemove2() {
		final Collection<String> collection = new ArrayList<>();
		collection.add("123foo");
		collection.add("12bar");

		final OrderedStringSet trie = new OrderedStringSet();
		Assert.assertTrue(trie.size() == 0);
		trie.addAll(collection);
		Assert.assertTrue(trie.size() == 2);

		trie.add("123fextra1");
		trie.add("123fextra2");
		trie.add("123fextra3");
		Assert.assertTrue(trie.size() == 5);

		trie.remove("123fextra1");
		trie.remove("123fextra2");
		trie.remove("123fextra3");

		Assert.assertTrue(trie.size() == 2);
	}

	@Test
	public void testRemoveAll() {
		final Collection<String> collection = new ArrayList<>();
		collection.add("123foo");
		collection.add("12bar");
		collection.add("poot");
		collection.add("poo");
		collection.add("123foo");
		collection.add("123foo");
		collection.add("12bar");

		final OrderedStringSet trie = new OrderedStringSet();
		Assert.assertTrue(trie.size() == 0);
		trie.addAll(collection);
		Assert.assertTrue(trie.size() == 4);

		trie.add("123fextra1");
		trie.add("123fextra2");
		trie.add("123fextra2");
		trie.add("123fextra3");
		trie.add("123fextra3");
		trie.add("123fextra3");
		Assert.assertTrue(trie.size() == 7);

		trie.removeAll(collection);
		Assert.assertTrue(trie.size() == 3);
	}

	@Test
	public void testRetainAll() {
		final Collection<String> collection = new ArrayList<>();
		collection.add("123foo");
		collection.add("12bar");
		collection.add("poot");
		collection.add("poo");
		collection.add("123foo");
		collection.add("123foo");
		collection.add("12bar");

		final OrderedStringSet trie = new OrderedStringSet();
		Assert.assertTrue(trie.size() == 0);
		trie.addAll(collection);
		Assert.assertTrue(trie.size() == 4);

		trie.add("123fextra1");
		trie.add("123fextra2");
		trie.add("123fextra2");
		trie.add("123fextra3");
		trie.add("123fextra3");
		trie.add("123fextra3");
		Assert.assertTrue(trie.size() == 7);

		trie.retainAll(collection);
		Assert.assertTrue(trie.size() == 4);
	}

	@Test
	public void testSimpleAddAndRemove() {
		final OrderedStringSet trie = new OrderedStringSet();
		Assert.assertTrue(trie.size() == 0);
		trie.add("123foo");

		Assert.assertTrue(trie.size() == 1);
		Assert.assertTrue(trie.contains("123foo"));
		trie.remove("123foo");
		Assert.assertTrue(trie.size() == 0);
		Assert.assertFalse(trie.contains("123foo"));

	}

}
