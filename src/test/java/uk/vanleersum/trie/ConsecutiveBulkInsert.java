/*******************************************************************************
 * Copyright 2018 Dr Phill van Leersum
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package uk.vanleersum.trie;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import uk.vanleersum.trie.uses.OrderedStringSet;

public class ConsecutiveBulkInsert {

	public static void main(final String[] args) {
		final int numberCount = 1000000;
		final int numberLength = 6;
		final int extraCount = 10000;

		System.out.println("Create basic list with " + numberCount + " strings of " + numberLength + " digits"); // DEBUG
		final List<String> numbers = ConsecutiveBulkInsert.CreateList(numberCount, numberLength);
		System.out.println("Create extra list with " + extraCount + " strings of " + numberLength + " digits"); // DEBUG
		final List<String> lookfor = ConsecutiveBulkInsert.CreateList(extraCount, numberLength);

		final OrderedStringSet trie = new OrderedStringSet();

		System.out.println("Insert initial contents");
		ConsecutiveBulkInsert.trieInsert(trie, numbers);

		System.out.println("Insert extras");
		ConsecutiveBulkInsert.trieInsert(trie, lookfor);

		System.out.println("Insert extras");
		ConsecutiveBulkInsert.trieInsert(trie, lookfor);

		System.out.println("Insert extras");
		ConsecutiveBulkInsert.trieInsert(trie, lookfor);

		System.out.println("Insert extras");
		ConsecutiveBulkInsert.trieInsert(trie, lookfor);

	}

	private static final List<String> CreateList(final int numberCount, final int numberLength) {
		final char[] source = new char[10];
		"0123456789".getChars(0, 10, source, 0);
		final Random rand = new Random();
		final List<String> numbers = new ArrayList<>();
		for (int i = 0; i < numberCount; i++) {
			String str = "";
			for (int j = 0; j < numberLength; j++) {
				final int ii = rand.nextInt(source.length);
				str += source[ii];
			}
			numbers.add(str);
		}
		return numbers;
	}

	private static void trieInsert(final OrderedStringSet trie, final List<String> numbers) {
		final long trieStart = System.currentTimeMillis();
		trie.addAll(numbers);
		final long trieFinished = System.currentTimeMillis();
		System.out.println("trieInsert = " + (trieFinished - trieStart) + "ms"); // DEBUG
	}

}
