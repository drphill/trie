/*******************************************************************************
 * Copyright 2018 Dr Phill van Leersum
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package uk.vanleersum.trie;

import java.util.Iterator;

import org.junit.Test;

import uk.vanleersum.trie.uses.OrderedStringSet;

public class TestIterator {

	@Test
	public void test() {
		final OrderedStringSet trie = new OrderedStringSet();
		trie.add("first");
		trie.add("second");
		trie.add("third");
		trie.add("first1");
		trie.add("first2");

		final Iterator<String> it = trie.iterator();
		while (it.hasNext()) {
			System.out.println("TestIterator.test: " + it.next()); // DEBUG
		}
	}

}
