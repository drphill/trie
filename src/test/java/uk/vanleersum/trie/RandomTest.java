package uk.vanleersum.trie;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import uk.vanleersum.trie.uses.StringIndexer;

public class RandomTest {
	Random random = new Random(0);

	@Test
	public void test() {
		int seed = 123456;
		for (int i = 0; i < 1000; i++) {
			final Set<String> stringSet = this.randomStringSet(seed);

			this.doit(stringSet);
			seed += 76543;
		}
	}

	private final void doit(final Set<String> stringSet) {
		final List<String> stringList = new ArrayList<>(stringSet);
		final IndexTrie<String> trie = new IndexTrie<>();
		final StringIndexer indexer = new StringIndexer("");
		// one by one add the strings to the list and check that we have all the strings
		// we expect
		for (int addIndex = 0; addIndex < stringList.size(); addIndex++) {
			final String string = stringList.get(addIndex);
			trie.putTrie(indexer.string(string)).value = string;
			for (int checkIndex = 0; checkIndex <= addIndex; checkIndex++) {
				final String getString = stringList.get(checkIndex);
				final IndexTrie<String> gotTrie = trie.getTrie(indexer.string(getString));
				Assert.assertNotNull(gotTrie);
				Assert.assertEquals(getString, gotTrie.value);
			}
		}

		// one by one remove the strings to the list and check that we have all the
		// strings we expect
		for (int removeIndex = 0; removeIndex < stringList.size(); removeIndex++) {
			final String stringToRemove = stringList.get(removeIndex);
			IndexTrie<String> gotTrie = trie.getTrie(indexer.string(stringToRemove));
			if (null != gotTrie) {
				gotTrie.value = null;
			}
			//// USED TO BE trie.removeValue(indexer.string(stringToRemove));
			//String removedString = trie.getValue(indexer.string(stringToRemove));
			//Assert.assertNull(removedString);

			for (int checkIndex = 0; checkIndex <= removeIndex; checkIndex++) {
				final String checkString = stringList.get(checkIndex);
				///final String removedString = trie.getValue(indexer.string(checkString));
				IndexTrie<String> gotTrie2 = trie.getTrie(indexer.string(stringToRemove));
				if (null != gotTrie2) {
					Assert.assertNull(gotTrie2.value);
				}
				
			}

			for (int checkIndex = removeIndex + 1; checkIndex < stringList.size(); checkIndex++) {
				final String getString = stringList.get(checkIndex);
				final IndexTrie<String> gotTrie2 = trie.getTrie(indexer.string(getString));
				Assert.assertNotNull(gotTrie2);
				Assert.assertEquals(getString, gotTrie2.value);
			}

		}

	}

	private Set<String> randomStringSet(final int seed) {
		// System.out.println("RandomTest.randomStringSet: " + seed); // DEBUG
		final Set<String> stringSet = new HashSet<>();
		this.random.setSeed(seed);
		for (int i = 0; i < 100; i++) {
			final String string = "999" + this.random.nextInt(1000);
			stringSet.add(string);
		}
		// System.out.println("RandomTest.randomStringSet: size = " + stringSet.size());
		// // DEBUG
		return stringSet;
	}

}
