/*******************************************************************************
 * Copyright 2018 Dr Phill van Leersum
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package uk.vanleersum.trie;

/**
 * Representation of the location of one trie with respect to another. This is
 * really only useful for debugging
 * 
 * @author phill
 *
 */
/* package */ final class Depth {
	public boolean found = false;
	public int logical = 0;
	public int physical = 0;

	@Override
	public String toString() {
		final StringBuilder buf = new StringBuilder();
		buf.append(this.found ? "Found" : "NotFound");
		buf.append(" at Logical: ");
		buf.append(this.logical);
		buf.append(", Physical: ");
		buf.append(this.physical);

		return buf.toString();
	}
}