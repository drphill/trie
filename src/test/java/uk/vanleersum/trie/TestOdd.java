package uk.vanleersum.trie;

import org.junit.Assert;
import org.junit.Test;

import uk.vanleersum.trie.uses.StringIndexer;

public class TestOdd {

	@Test
	public void test() {

		final IndexTrie<String> tags = new IndexTrie<>();
		final StringIndexer indexer = new StringIndexer("");

		tags.putTrie(indexer.string("UK0123456 00070")).value = "UK0123456 00070";
		assertTrieContains(tags, "UK0123456 00070");

		tags.putTrie(indexer.string("UK0123456 00092")).value = "UK0123456 00092";
		assertTrieContains(tags, "UK0123456 00070");
		assertTrieContains(tags, "UK0123456 00092");

		tags.putTrie(indexer.string("UK0123456 00100")).value = "UK0123456 00100";
		assertTrieContains(tags, "UK0123456 00070");
		assertTrieContains(tags, "UK0123456 00092");
		assertTrieContains(tags, "UK0123456 00100");

	}

	@Test
	public void test2() {

		final IndexTrie<String> tags = new IndexTrie<>();
		final StringIndexer indexer = new StringIndexer("");

		tags.putTrie(indexer.string("00070")).value = "00070";
		assertTrieContains(tags, "00070");

		tags.putTrie(indexer.string("00092")).value = "00092";
		assertTrieContains(tags, "00070");
		assertTrieContains(tags, "00092");

		tags.putTrie(indexer.string("00100")).value = "00100";
		assertTrieContains(tags, "00070");
		assertTrieContains(tags, "00092");
		assertTrieContains(tags, "00100");

	}

	@Test
	public void test3() {

		final IndexTrie<String> tags = new IndexTrie<>();
		final StringIndexer indexer = new StringIndexer("");

		tags.putTrie(indexer.string("007")).value = "007";
		assertTrieContains(tags, "007");

		tags.putTrie(indexer.string("009")).value = "009";
		assertTrieContains(tags, "007");
		assertTrieContains(tags, "009");

		tags.putTrie(indexer.string("010")).value = "010";
		assertTrieContains(tags, "007");
		assertTrieContains(tags, "009");
		assertTrieContains(tags, "010");

	}

	@Test
	public void test4() {

		final IndexTrie<String> strings = new IndexTrie<>();
		final StringIndexer indexer = new StringIndexer("");

		strings.putTrie(indexer.string("001")).value = "001";
		assertTrieContains(strings, "001");
		// System.out.println(strings.debugString());

		strings.putTrie(indexer.string("002")).value = "002";
		assertTrieContains(strings, "001");
		assertTrieContains(strings, "002");
		// System.out.println(strings.debugString());

		strings.putTrie(indexer.string("010")).value = "010";
		// System.out.println(strings.debugString());
		assertTrieContains(strings, "001");
		assertTrieContains(strings, "002");
		assertTrieContains(strings, "010");

	}
	
	private void assertTrieContains(IndexTrie<String> trie, String value) {
		IndexTrie<String> got = trie.getTrie(new StringIndexer(value)) ;
		Assert.assertNotNull(got);
		Assert.assertEquals(value, got.value);
	}
}
