/*******************************************************************************
 * Copyright 2018 Dr Phill van Leersum
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package uk.vanleersum.trie;

import org.junit.Test;

import uk.vanleersum.trie.uses.StringIndexer;

public class TestCompact {

	@Test
	public void test() {
		final IndexTrie<String> trie = new IndexTrie<>();
		trie.putTrie(new StringIndexer("1111")).value = "1111";
		System.out.println(trie.debugString());
		trie.putTrie(new StringIndexer("1112")).value = "1112";
		System.out.println(trie.debugString());
		//trie.removeValue(new StringIndexer("1112"));
		//System.out.println(trie.debugString());
	}

}
