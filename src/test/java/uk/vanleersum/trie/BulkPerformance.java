/*******************************************************************************
 * Copyright 2018 Dr Phill van Leersum
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package uk.vanleersum.trie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import uk.vanleersum.trie.uses.OrderedStringSet;

public class BulkPerformance {

	public static void main(final String[] args) {
		final int numberCount = 100000;
		final int numberLength = 6;
		final int extraCount = 1000;

		System.out.println("Create basic list with " + numberCount + " strings of " + numberLength + " digits"); // DEBUG
		final List<String> numbers = BulkPerformance.CreateList(numberCount, numberLength);
		System.out.println("Create extra list with " + extraCount + " strings of " + numberLength + " digits"); // DEBUG
		final List<String> lookfor = BulkPerformance.CreateList(extraCount, numberLength);

		// Create lists
		final List<String> arrayList = new ArrayList<>();

		final OrderedStringSet trie = new OrderedStringSet();

		System.out.println("Insert basic list into collections"); // DEBUG
		BulkPerformance.arrayListInsert(arrayList, numbers);
		BulkPerformance.trieInsert(trie, numbers);

		System.out.println("\nLook for members of extra list in collections"); // DEBUG
		BulkPerformance.arrayListSearch(arrayList, lookfor);
		BulkPerformance.trieSearch(trie, lookfor);

		System.out.println("\nInsert extra list into collections"); // DEBUG
		BulkPerformance.arrayListInsert(arrayList, lookfor);
		BulkPerformance.trieInsert(trie, lookfor);

		System.out.println("\nLook for members of extra list in collections"); // DEBUG
		BulkPerformance.arrayListSearch(arrayList, lookfor);
		BulkPerformance.trieSearch(trie, lookfor);

		System.out.println("\nRemove members of extra list from collections"); // DEBUG
		BulkPerformance.arrayListRemove(arrayList, lookfor);
		BulkPerformance.trieRemove(trie, lookfor);

		System.out.println("\nRemove members of extra list from collections"); // DEBUG
		BulkPerformance.arrayListRemove(arrayList, lookfor);
		BulkPerformance.trieRemove(trie, lookfor);

		System.out.println("\nRemove members of extra list from collections"); // DEBUG
		BulkPerformance.arrayListRemove(arrayList, lookfor);
		BulkPerformance.trieRemove(trie, lookfor);

		// System.out.println("Test.main: \n"+ trie.treeString()); // DEBUG
		//
		// final String find = "78";
		// final Trie t = trie.find(find);
		// System.out.println("\nTest.main: find("+find+") -> "+ t); // DEBUG
		//
		// System.out.println("\n Test.main:sortedEndpoints"); // DEBUG
		// for (final Trie tt : trie.sortedEndPoints()) {
		// System.out.println(tt); // DEBUG
		// }

	}

	private static void arrayListInsert(final List<String> arrayList, final List<String> numbers) {
		final long arrayListStart = System.currentTimeMillis();
		arrayList.addAll(numbers);
		final long arrayListAdded = System.currentTimeMillis();
		Collections.sort(arrayList);
		final long arrayListSorted = System.currentTimeMillis();
		System.out.println("arrayListInsert = " + (arrayListSorted - arrayListStart) + "ms"); // DEBUG
		System.out.println("arrayListInsert add = " + (arrayListAdded - arrayListStart) + "ms"); // DEBUG
		System.out.println("arrayListInsert sort = " + (arrayListSorted - arrayListAdded) + "ms"); // DEBUG
	}

	private static void arrayListRemove(final List<String> arrayList, final List<String> remove) {
		final long start = System.currentTimeMillis();
		arrayList.removeAll(remove);
		final long end = System.currentTimeMillis();
		System.out.println("arrayList removed in " + (end - start) + "ms"); // DEBUG
	}

	private static void arrayListSearch(final List<String> arrayList, final List<String> lookfor) {
		final long start = System.currentTimeMillis();
		final boolean contains = arrayList.containsAll(lookfor);
		final long end = System.currentTimeMillis();
		System.out.println("arrayListSearch: foundAll " + contains + " in " + (end - start) + "ms"); // DEBUG
	}

	private static final List<String> CreateList(final int numberCount, final int numberLength) {
		final char[] source = new char[10];
		"0123456789".getChars(0, 10, source, 0);
		final Random rand = new Random();
		final List<String> numbers = new ArrayList<>();
		for (int i = 0; i < numberCount; i++) {
			String str = "";
			for (int j = 0; j < numberLength; j++) {
				final int ii = rand.nextInt(source.length);
				str += source[ii];
			}
			numbers.add(str);
		}
		return numbers;
	}

	private static void trieInsert(final OrderedStringSet trie, final List<String> numbers) {
		final long trieStart = System.currentTimeMillis();
		trie.addAll(numbers);
		final long trieFinished = System.currentTimeMillis();
		System.out.println("trieInsert = " + (trieFinished - trieStart) + "ms"); // DEBUG
	}

	private static void trieRemove(final OrderedStringSet trie, final List<String> remove) {
		final long start = System.currentTimeMillis();
		trie.removeAll(remove);
		final long end = System.currentTimeMillis();
		System.out.println("trie removed in " + (end - start) + "ms"); // DEBUG
	}

	private static void trieSearch(final OrderedStringSet trie, final List<String> lookfor) {
		final long start = System.currentTimeMillis();
		final boolean trieFound = trie.containsAll(lookfor);
		final long end = System.currentTimeMillis();
		System.out.println("trieSearch: found " + trieFound + " in " + (end - start) + "ms"); // DEBUG
	}

}
