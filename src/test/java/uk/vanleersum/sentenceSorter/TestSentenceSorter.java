package uk.vanleersum.sentenceSorter;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import uk.vanleersum.comparableTrie.uses.SentenceSorter;

public class TestSentenceSorter {

	@Test
	public void test() {
		SentenceSorter sorter = new SentenceSorter();
		
		sorter.add("This is a sentence.");
		sorter.add("This is another sentence.");
		sorter.add("A sentence that should come first.");
		sorter.add("Ze last sentence");
		sorter.add("This is a sentence that comes somewhere in the middle.");
		sorter.add("This is another sentence entirely.");
		
		Iterator<String> it = sorter.iterator();
		while (it.hasNext()) {
			System.out.println(it.next()); 
		}
		
		// Notice that 'a' does not match 'another'
		it = sorter.sentencesWithPrefix("This is a").iterator();
		while (it.hasNext()) {
			System.out.println(it.next()); 
		}
		
		it = sorter.sentencesWithPrefix("This is another").iterator();
		while (it.hasNext()) {
			System.out.println(it.next()); 
		}
	}

}
