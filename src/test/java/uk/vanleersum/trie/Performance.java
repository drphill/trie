/*******************************************************************************
 * Copyright 2018 Dr Phill van Leersum
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package uk.vanleersum.trie;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import uk.vanleersum.trie.uses.OrderedStringSet;

public class Performance {

	public static void main(final String[] args) {
		final int numberCount = 1000000;
		final int numberLength = 6;
		final int extraCount = 1000000;

		System.out.println("Create basic list with " + numberCount + " strings of " + numberLength + " digits");
		final List<String> numbers = Performance.CreateList(numberCount, numberLength, 1234567890L);
		System.out.println("Create extra list with " + extraCount + " strings of " + numberLength + " digits");
		final List<String> lookfor = Performance.CreateList(extraCount, numberLength, 9876543210L);

		// Create lists
		// final Set<String> set = new HashSet<>();
		final Set<String> set = new TreeSet<>();
		// final Set<String> set = new StringTrie("1234567890");

		final Set<String> trie = new OrderedStringSet();

		System.out.print("Insert basic list:   ");
		long setTime = Performance.setInsert(set, numbers);
		long trieTime = Performance.setInsert(trie, numbers);
		System.out.println(" " + ((trieTime * 100) / setTime) + "% [" + trieTime + " / " + setTime + "] ");

		System.out.println("clear collections");
		set.clear();
		trie.clear();

		System.out.print("Insert basic list:   ");
		setTime = Performance.setInsert(set, numbers);
		trieTime = Performance.setInsert(trie, numbers);
		System.out.println(" " + ((trieTime * 100) / setTime) + "% [" + trieTime + " / " + setTime + "] ");

		System.out.print("Look for extra list: ");
		setTime = Performance.setSearch(set, lookfor);
		trieTime = Performance.setSearch(trie, lookfor);
		System.out.println(" " + ((trieTime * 100) / setTime) + "% [" + trieTime + " / " + setTime + "] ");

		System.out.print("Look for extra list: ");
		setTime = Performance.setSearch(set, lookfor);
		trieTime = Performance.setSearch(trie, lookfor);
		System.out.println(" " + ((trieTime * 100) / setTime) + "% [" + trieTime + " / " + setTime + "] ");

		System.out.print("Insert extra list:   ");
		setTime = Performance.setInsert(set, lookfor);
		trieTime = Performance.setInsert(trie, lookfor);
		System.out.println(" " + ((trieTime * 100) / setTime) + "% [" + trieTime + " / " + setTime + "] ");

		System.out.print("Insert extra list:   ");
		setTime = Performance.setInsert(set, lookfor);
		trieTime = Performance.setInsert(trie, lookfor);
		System.out.println(" " + ((trieTime * 100) / setTime) + "% [" + trieTime + " / " + setTime + "] ");

		System.out.print("Look for extra list: ");
		setTime = Performance.setSearch(set, lookfor);
		trieTime = Performance.setSearch(trie, lookfor);
		System.out.println(" " + ((trieTime * 100) / setTime) + "% [" + trieTime + " / " + setTime + "] ");

		System.out.print("Remove extra list:   ");
		setTime = Performance.setRemove(set, lookfor);
		trieTime = Performance.setRemove(trie, lookfor);
		System.out.println(" " + ((trieTime * 100) / setTime) + "% [" + trieTime + " / " + setTime + "] ");

		System.out.print("Remove extra list:   ");
		setTime = Performance.setRemove(set, lookfor);
		trieTime = Performance.setRemove(trie, lookfor);
		System.out.println(" " + ((trieTime * 100) / setTime) + "% [" + trieTime + " / " + setTime + "] ");

		System.out.print("Insert extra list:   ");
		setTime = Performance.setInsert(set, lookfor);
		trieTime = Performance.setInsert(trie, lookfor);
		System.out.println(" " + ((trieTime * 100) / setTime) + "% [" + trieTime + " / " + setTime + "] ");

		System.out.print("Remove extra list:   ");
		setTime = Performance.setRemove(set, lookfor);
		trieTime = Performance.setRemove(trie, lookfor);
		System.out.println(" " + ((trieTime * 100) / setTime) + "% [" + trieTime + " / " + setTime + "] ");

		System.out.print("Remove extra list:   ");
		setTime = Performance.setRemove(set, lookfor);
		trieTime = Performance.setRemove(trie, lookfor);
		System.out.println(" " + ((trieTime * 100) / setTime) + "% [" + trieTime + " / " + setTime + "] ");

	}

	private static final List<String> CreateList(final int numberCount, final int numberLength, final long seed) {
		final char[] source = new char[10];
		"0123456789".getChars(0, 10, source, 0);
		final Random rand = new Random(seed);
		final List<String> numbers = new ArrayList<>();
		for (int i = 0; i < numberCount; i++) {
			String str = "";
			for (int j = 0; j < numberLength; j++) {
				final int ii = rand.nextInt(source.length);
				str += source[ii];
			}
			numbers.add(str);
		}
		return numbers;
	}

	private static long setInsert(final Set<String> set, final List<String> numbers) {
		System.gc();
		final long setStart = System.currentTimeMillis();
		for (final String word : numbers) {
			set.add(word);
		}
		final long setAdded = System.currentTimeMillis();
		final long time = setAdded - setStart;
		// System.out.println("setInsert add = " + time + "ms");
		return time;
	}

	private static long setRemove(final Set<String> set, final List<String> remove) {
		System.gc();
		final long start = System.currentTimeMillis();
		int arrayFound = 0;
		for (final String look : remove) {
			if (set.remove(look)) {
				arrayFound++;
			}
		}
		final long end = System.currentTimeMillis();
		final long time = end - start;
		// System.out.println("set: removed "+arrayFound+ " in "+time + "ms");
		return time;
	}

	private static long setSearch(final Set<String> set, final List<String> lookfor) {
		System.gc();
		final long start = System.currentTimeMillis();
		int arrayFound = 0;
		for (final String look : lookfor) {
			if (set.contains(look)) {
				arrayFound++;
			}
		}
		final long end = System.currentTimeMillis();
		final long time = end - start;
		// System.out.println("setSearch: found "+arrayFound+ " in "+time + "ms");
		return time;
	}

}
