/*******************************************************************************
 * Copyright 2018 Dr Phill van Leersum
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package uk.vanleersum.trie.uses;

import java.util.Iterator;

public class StringIndexer implements Iterator<Integer> {
	// TODO: can use a 'template' string to reduce the range of the ints returned?
	private String string;
	private int index = 0;

	private final int[] map;

	public StringIndexer(final String string) {
		this(string, null);
	}

	public StringIndexer(final String string, final int[] map) {
		this.string = string;
		this.map = map;
	}

	@Override
	public boolean hasNext() {
		return this.index < this.string.length();
	}

	@Override
	public Integer next() {
		final int charAt = this.string.charAt(this.index++);
		if (null == this.map) {
			return charAt;
		}
		return this.map[charAt];
	}

	public final String string() {
		return this.string;
	}

	public final StringIndexer string(final String string) {
		this.string = string;
		this.index = 0;
		return this;
	}

}
