package uk.vanleersum.comparableTrie.uses;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import uk.vanleersum.comparableTrie.ComparableTrie;

public class SentenceSorter implements Set<String> {
	private ComparableTrie<String> trie = new ComparableTrie<>();
	
	
	public SentenceSorter() { }
	
	@Override
	public final boolean add(final String string) {
		this.trie.putTrie(new SentenceIndexer(string)).value = string;
		return true;
	}

	@Override
	public boolean addAll(final Collection<? extends String> c) {
		for (final String string : c) {
			this.add(string);
		}
		return true;
	}

	@Override
	public final void clear() {
		this.trie.clear();
	}

	@Override
	public final boolean contains(final Object o) {
		if (o instanceof String) {
			final String string = (String) o;
			return this.contains(string);
		}
		return false;
	}

	public final boolean contains(final String string) {
		final ComparableTrie<String> t = this.trie.getTrie(new SentenceIndexer(string));
		if (null != t) {
			return true;
		}
		return false;
	}

	@Override
	public boolean containsAll(final Collection<?> c) {
		for (final Object o : c) {
			if (!this.contains(o)) {
				return false;
			}
		}
		return true;
	}

	public String debugString() {
		return this.trie.debugString();
	}

	@Override
	public boolean isEmpty() {
		return 0 == this.size();
	}

	@Override
	public final Iterator<String> iterator() {
		return this.trie.iterator();
	}

	@Override
	public final boolean remove(final Object o) {
		if (o instanceof String) {
			this.remove((String) o);
			return true;
		}
		return false;
	}

	public final void remove(final String string) {
		final ComparableTrie<String> t = this.trie.getTrie(new SentenceIndexer(string));
		if (null != t) {
			t.remove();
		}
	}

	@Override
	public boolean removeAll(final Collection<?> c) {
		for (final Object o : c) {
			this.remove(o);
		}
		return true;
	}

	@Override
	public boolean retainAll(final Collection<?> c) {
		for (final String string : this.trie.list()) {
			if (!c.contains(string)) {
				this.remove(string);
			}
		}
		return false;
	}

	@Override
	public final int size() {
		return this.trie.size();
	}

	/**
	 * @param prefix
	 * @return an ordered list of the strings that begin with the supplied prefix.
	 */
	public final List<String> sentencesWithPrefix(final String prefix) {
		final ComparableTrie<String> t = this.trie.getTrie(new SentenceIndexer(prefix));
		if (null == t) {
			return new ArrayList<>();
		}
		return t.list();
	}

	@Override
	public final Object[] toArray() {
		return this.trie.list().toArray();
	}

	@Override
	public final <T> T[] toArray(final T[] a) {
		return this.trie.list().toArray(a);
	}

}
